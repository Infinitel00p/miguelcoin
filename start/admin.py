from django.contrib import admin

# Register your models here.
from .models import Price, Coin, Exchange

admin.site.register(Price)
admin.site.register(Coin)
admin.site.register(Exchange)
