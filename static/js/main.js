$(document).ready(function() { 

    // $(".internet").hide();

    var m,s;
    function getMinutos(time) {
        m = Math.floor( time / 60 );
        s = time % 60;

        if (m>=1)
            s+=60;
        else 
            if (m<1)
                m = 1;
    }
    getMinutos(lapse);

    var lapse = $('#timelapse').val();
    $('#timelapse').change(() => {
        clearTimeout(x);
        lapse = $('#timelapse').val();
        getMinutos(lapse);
        countdown(1,30,true);
    });

    var currency = $('#currency').val();
    $('#currency').change(() => {
        clearTimeout(x);
        currency = $('#currency').val();
        switch(currency) {
            case "USDT":
                $('.pr').text("Price ($)");
                $('.op').text("Open ($)");
                break;
            case "BTC":
                $('.pr').text("Price (BTC)");
                $('.op').text("Open (BTC)");
                break;
            case "BNB":
                $('.pr').text("Price (BNB)");
                $('.op').text("Open (BTC)");
                break;
            case "ETH":
                $('.pr').text("Price (ETH)");
                $('.op').text("Open (BTC)");
                break;
        }

        getMinutos(lapse);
        countdown(1,30,true);
    });

    var exchange = "bittrex";
    $('#bittrex').change(() => {
        clearTimeout(x);
        if ($('#bittrex').is(":checked"))
            exchange = "bittrex";
        else
            exchange = "binance";

        getMinutos(lapse);

        countdown(1,30, true);
    });
    $('#binance').change(() => {
        clearTimeout(x);
        if ($('#binance').is(":checked"))
            exchange = "binance";
        else
            exchange = "bittrex";

        getMinutos(lapse);
        countdown(1,30, true);
    });


    var x ;
    function countdown(minutes, segundos, enew) {
        if (navigator.onLine) {
            if (enew){
                var url = "/inicio/api/prices?base="+currency+"&time="+lapse+"&exchange="+exchange;
                $.ajax({
                  url: url,
                  type: "GET",
                  dataType: 'json',
                }).then((response) => {
                    if(response.err){
                        console.log(response.message);
                    }else{
                        llenarTabla(response);
                        // checkinternet(false);
                    }
                }).catch(e => {console.log(e)
                        // checkinternet(true);
                    })
            } 
        }
        
        var seconds = segundos;
        var mins = minutes
        function tick() {
            //This script expects an element with an ID = "counter". You can change that to what ever you want. 
            var counter = document.getElementById("demo");
            var current_minutes = mins-1
            seconds--;
            counter.innerHTML = current_minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds);
            if( seconds > 0 ) {
                x = setTimeout(tick, 1000);
            } else {
                if(mins > 1){
                    countdown(mins-1,segundos, false);           
                }else{
                    countdown(1, 30, true);
                }
            }
        }
        tick();
    }

     
    function llegar() {
        if (navigator.onLine){
            $.ajax({
              url: "/inicio/api/setprices",
              type: "GET",
              dataType: 'json',
            }).then((response) => {
                if (response.err){
                    console.log(err.message);
                }else{
                    $("#bitcoin").text(response["price"]);
                    // checkinternet(false);
                }
            }).catch(e => {console.log(e); 
                // checkinternet(true); 
            })
            
            y = setTimeout(llegar, 20000);
        }
    }
    
    llegar();

    //You can use this script with a call to onclick, onblur or any other attribute you would like to use. 
    countdown(1, 30, true);//where n is the number of minutes required. 
    

    $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".tg tr.data-row").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
        $(".tl tr.data-row").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });


    function timer(t) {
        var h = t;
        function time_s() {						
            h ++;
            $('#timer').text(Math.floor(h / 3600) +":"+ Math.floor(h % 3600 / 60) + ":" +Math.floor(h % 3600 % 60));	
            setTimeout(time_s, 1000);
        }
        time_s();
    }
    
    timer(0);
    
}); 


/* function checkinternet(status) {
    if (!status)
        $('.internet').hide();
    else
        $('.internet').show();
} */

function llenarTabla(data) {
$('tbody').text("");
$.each(data.positives, function( index, value ) {
    var text;
    if ( Math.sign(value["change"]) == 1 ) {
        text =''+
            '<tr class="data-row">'+
                '<td> <a target="_blank" href="https://bittrex.com/Market/Index?MarketName='+value["MarketName"]+'">'+value["symbol"]+'</a></td>'+
                '<td>'+value["price"]+'</td>'+
                '<td>'+value["open"]+'</td>'+
                '<td class="gain"> <span>'+parseFloat(value["change"])+'</span></td>'+
            '</tr>'+
        '';
        $('.table-gain').append(text);
    }else{
        text =''+
            '<tr class="data-row">'+
                '<td> <a target="_blank" href="https://bittrex.com/Market/Index?MarketName='+value["MarketName"]+'">'+value["symbol"]+'</a></td>'+
                '<td>'+value["price"]+'</td>'+
                '<td>'+value["open"]+'</td>'+
                '<td> <span>'+parseFloat(value["change"])+'</span></td>'+
            '</tr>'+
        '';
        $('.table-gain').append(text);
    }
      
});

$.each(data.negatives, function( index, value ) {
    var text;
    text =''+
        '<tr class="data-row">'+
            '<td> <a target="_blank" href="https://bittrex.com/Market/Index?MarketName='+value["MarketName"]+'">'+value["symbol"]+'</a></td>'+
            '<td>'+value["price"]+'</td>'+
            '<td>'+value["open"]+'</td>'+
            '<td class="loss"> <span>'+parseFloat(value["change"])+'</span></td>'+
        '</tr>'+
    '';
    $('.table-loss').append(text);
      
});

$(".tg").tablesorter();
$(".tl").tablesorter(); 
}