from datetime import datetime,timedelta
import requests
from celery import shared_task
from django.utils import timezone
from django.db import connection
from miguelcoin.celery import app
from .models import *

@shared_task
def deleteDB():
	cursor = connection.cursor()
	# cursor.execute("TRUNCATE TABLE start_price")
	cursor.execute("DELETE FROM start_price")

@shared_task
def setPriceBittrex():
	now = datetime.now(tz=timezone.utc)
	response = requests.get("https://bittrex.com/api/v1.1/public/getmarketsummaries")
	if response.status_code == 200:
		results = response.json()
		for r in results["result"]:
			try:
				s = r["MarketName"].split("-",2)
				c = Coin.objects.get(co_name=s[1]+s[0])
				idExCo = ExchangeCoin.objects.get(coin=c, exchange = "bittrex")
				price = Price.objects.create(pr_time_stamp=now, pr_price=r["Last"],coin_exchange=idExCo)
			except Exception as e:
				print(str(e))
	



@shared_task
def setPriceBinance():
	now = datetime.now(tz=timezone.utc)
	response = requests.get("https://api.binance.com/api/v3/ticker/price")
	if response.status_code == 200:
		results = response.json()
		for r in results:
			try:
				c = Coin.objects.get(co_name=r["symbol"])
				idExCo = ExchangeCoin.objects.get(coin=c, exchange="binance")
				price = Price.objects.create(pr_time_stamp=now, pr_price=r["price"],coin_exchange=idExCo)
			except Exception as e:
				print(str(e))

@shared_task
def init():
	binance, created_bin = Exchange.objects.get_or_create(pk='binance')
	bittrex, created_bit = Exchange.objects.get_or_create(pk='bittrex')

	response = requests.get("https://bittrex.com/api/v1.1/public/getmarkets")
	if response.status_code == 200:
		results = response.json()
		for r in results["result"]:
			c, created_c = Coin.objects.get_or_create(co_name = r["MarketCurrency"]+r["BaseCurrency"], co_base_currency= r["BaseCurrency"])
			if created_c:
				exco = ExchangeCoin(exchange=bittrex, coin=c)
				exco.save()
			else:
				if ( not ExchangeCoin.objects.filter(exchange=bittrex,coin=c).exists()):
					exco = ExchangeCoin(exchange=bittrex, coin=c)
					exco.save()

	response = requests.get("https://api.binance.com/api/v3/ticker/price")	
	if response.status_code == 200:
		results = response.json()
		for r in results:

			if (r['symbol'][-3:] == 'BTC'):
				base = 'BTC'
			elif (r['symbol'][-3:] == 'ETH'):
				base = 'ETH'
			elif (r['symbol'][-4:] == 'USDT'):
				base = 'USDT'
			elif(r['symbol'][-3:] == 'PAX'):
				base = 'PAX'
			elif(r['symbol'][-4:] == 'TUSD'):
				base = 'TUSD'
			elif(r['symbol'][-3:] == 'BNB'):
				base = 'BNB'
			elif(r['symbol'][-4:] == 'USDC'):
				base = 'USDC'
			else:
				base = False

			if (base):
				c, created_c = Coin.objects.get_or_create(co_name = r["symbol"], co_base_currency=base)
				if created_c:
					exco = ExchangeCoin(exchange=binance, coin=c)
					exco.save()
				else:
					if ( not ExchangeCoin.objects.filter(exchange=binance,coin=c).exists() ):
						exco = ExchangeCoin(exchange=binance, coin=c)
						exco.save()



app.conf.beat_schedule = {
	'setprices-setPriceBittrex-every-20-seconds': {
	    'task': 'start.tasks.setPriceBittrex',
	    'schedule': timedelta(seconds=20)
	},


	'setprices-setPriceBinance-every-20-seconds': {
	    'task': 'start.tasks.setPriceBinance',
	    'schedule': timedelta(seconds=20)
	},

	'setprices-setPriceBinance-every-2-days': {
		'task': 'start.tasks.init',
		'schedule': timedelta(days=2)
	},

	
	'deleteDB-every-one-hour': {
	    'task': 'start.tasks.deleteDB',
	    'schedule': timedelta(seconds=3600)
	},
}