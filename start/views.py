import pprint
import requests
import json, urllib.request
from operator import itemgetter
from datetime import datetime, timedelta
from django.utils import timezone
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, JsonResponse 
from .models import *

def init(request):
	return render(request, 'start/index.html', {})




def getPrices(request):
	base = request.GET.get('base')
	time = request.GET.get('time')
	exchange = request.GET.get('exchange')

	# print (base)
	# print (time)
	# print (exchange)
	data_positive = []
	data_negative = []
	exc = Exchange.objects.get(ex_name=exchange)

	now = datetime.now(tz=timezone.utc)
	before = now-timedelta(seconds=int(time)+10)
	after = now-timedelta(seconds=int(time)-10)
	# print ("")
	# print (now)
	# print (before)
	# print (after)
	# print ("")
	ex = Exchange.objects.get(pk=exchange)
	# try:
	if (exchange == "bittrex"):
		response = requests.get("https://bittrex.com/api/v1.1/public/getmarketsummaries")
		if response.status_code == 200:
			results = response.json()
			for r in results["result"]:
				try:
					s = r["MarketName"].split("-",2)
					c = Coin.objects.get(co_name=s[1]+s[0])
					if (str(c.co_base_currency) == base):
						idExCo = ExchangeCoin.objects.get(coin=c, exchange = exchange)
						price = Price.objects.filter(pr_time_stamp__range=[before,after], coin_exchange=idExCo).order_by('-pr_time_stamp')#
						last = float(r["Last"])
						if (price.exists()):
							precio = price[0].pr_price 						
						else:
							precio = last

						change = round(((float(last)*100)/float(precio))-100,2)
						if (change >= 0):
							data_positive.append({
							'symbol':s[1]+s[0],
							'price':round(last,8),
							'open':round(precio,8),
							'change':change,
							'MarketName': r["MarketName"],
							})
						else :
							data_negative.append({
							'symbol':s[1]+s[0],
							'price':round(last,8),
							'open':round(precio,8),
							'change':change,
							'MarketName': r["MarketName"],
							})

				except Exception as e:
					print(str(e))
					# return JsonResponse({'err': True, 'message': 'Unexpected error'}, safe = False)
			
			data_positive = sorted(data_positive, key=itemgetter('change'), reverse=True)
			data_negative = sorted(data_negative, key=itemgetter('change'), reverse=False)
			
	if (exchange == "binance"):
		response = requests.get("https://api.binance.com/api/v3/ticker/price")
		if response.status_code == 200:
			results = response.json()
			for r in results:
				try:
					c = Coin.objects.get(co_name=r["symbol"])
					if (str(c.co_base_currency) == base):
						idExCo = ExchangeCoin.objects.get(coin=c, exchange = exchange)
						price = Price.objects.filter(pr_time_stamp__range=[before,after], coin_exchange=idExCo).order_by('-pr_time_stamp')#
						last = float(r["price"])

						if (price.exists()):
							precio = price[0].pr_price 
						else:
							precio = last
						
						change = round(((float(last*100))/float(precio))-100, 2)
						if (change >= 0):
							data_positive.append({
							'symbol':r["symbol"],
							'open':round(precio,8),
							'price':round(last,8),
							'change':change,
							})
						else :
							data_negative.append({
							'symbol':r["symbol"],
							'open':round(precio,8),
							'price':round(last,8),
							'change':change,
							})

				except Exception as e:
					print(r["symbol"])
					print(str(e))
					# return JsonResponse({'err': True, 'message': 'Unexpected error'}, safe = False)

			data_positive = sorted(data_positive, key=itemgetter('change'), reverse=True)
			data_negative = sorted(data_negative, key=itemgetter('change'), reverse=False)
	return JsonResponse({'err': False, 'positives':data_positive, 'negatives': data_negative}, safe = False)
	

	
from .tasks import setPriceBinance
@csrf_exempt
def setPrices(request):
	response = requests.get("https://api.bitfinex.com/v1/pubticker/btcusd")
	
	if response.status_code == 200:
		results = response.json()
		data = {
			'price':results["last_price"],
			'err':False
		}
	else:
		data = {
			'message':"Unexpected error",
			'err':True
		}

	return JsonResponse(data, safe = False)

	