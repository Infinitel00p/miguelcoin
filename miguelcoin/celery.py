from __future__ import absolute_import, unicode_literals
import os
from dotenv import load_dotenv
from celery import Celery

load_dotenv()

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'miguelcoin.settings')
app = Celery('miguelcoin') 
app.config_from_object('django.conf:settings', namespace='CELERY') 
app.autodiscover_tasks()
app.conf.update( 
    BROKER_URL = os.getenv('BROKER_URL'),
)
print(os.getenv('BROKER_URL'))
# app.conf.timezone = 'VET'