from django.db import models

# Create your models here.
class Coin (models.Model):
	CURRENCY = (
        ('dollar', 'USDT'),
        ('BNB', 'BNB'),
        ('BTC', 'BTC'),
        ('TUSD', 'TUSD'),
        ('ETH', 'ETH'),
        ('PAX', 'PAX'),
        ('BNB', 'BNB'),
        ('USDC', 'USDC'),
    )

	id = models.AutoField(primary_key=True)
	co_name = models.CharField(max_length=10)
	co_base_currency = models.CharField(max_length=4, choices=CURRENCY)
	class Meta:
		 unique_together = ('co_name', 'co_base_currency',)
	
class Exchange(models.Model):
	ex_name = models.CharField(max_length=40, primary_key=True)
	coins = models.ManyToManyField(Coin,through='ExchangeCoin', symmetrical = True, blank=True)


	

class ExchangeCoin (models.Model):
    coin = models.ForeignKey(Coin, on_delete=models.CASCADE)
    exchange = models.ForeignKey(Exchange, on_delete=models.CASCADE)
    class Meta:
    	unique_together = ('coin', 'exchange',)

class Price(models.Model):
	id = models.AutoField(primary_key=True)
	pr_time_stamp = models.DateTimeField()
	pr_price = models.DecimalField(max_digits=24, decimal_places=12)
	coin_exchange = models.ForeignKey(ExchangeCoin, on_delete=models.CASCADE, blank=False, null=False)