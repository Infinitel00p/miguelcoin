from django.conf.urls import url
from .views import *

urlpatterns = [
	url(r'^$', init, name='init'),
	url(r'^api/prices$', getPrices, name='prices'),
	url(r'^api/setprices$', setPrices, name='prices'),
]